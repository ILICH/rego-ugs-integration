using REGO.Game;

namespace REGO.Infrastructure
{
    public interface ICommand : IMessage { }
}