using System;
using System.Collections.Generic;
using ClassLibrary1.Payment;
using ClassLibrary1.Services.Bonus.Commands;
using ClassLibrary1.Services.Payment.Commands;
using REGO.Bonus;
using REGO.Infrastructure;

namespace REGO.Game
{
    class MessageBus
    {
        private readonly Func<PaymentSubscriber> _paymentSubscriberFactory;
        private readonly Func<BonusSubscriber> _bonusSubscriberFactory;

        readonly List<IMessage> _messages = new List<IMessage>();

        public MessageBus(Func<PaymentSubscriber> paymentSubscriberFactory, Func<BonusSubscriber> bonusSubscriberFactory )
        {
            _paymentSubscriberFactory = paymentSubscriberFactory;
            _bonusSubscriberFactory = bonusSubscriberFactory;
        }

        public IEnumerable<IMessage> Messages => _messages;

        public void Publish(IEvent @event)
        {
            PublishMessage(@event);
        }

        public void Publish(ICommand command)
        {
            PublishMessage(command);
        }

        private void PublishMessage(IMessage message)
        {
            _messages.Add(message);

            //some dirty hardcoded message dispatching just to make prototype work

            //payment
            if (message is Deposit)
                _paymentSubscriberFactory().Handle((Deposit)message);
            else if (message is WithdrawRequestSubmit)
                _paymentSubscriberFactory().Handle((WithdrawRequestSubmit)message);
            else if (message is WithdrawRequestApprove)
                _paymentSubscriberFactory().Handle((WithdrawRequestApprove)message);
            

            //bonus
            else if (message is ApplyForWageringRequirementBonus)
                _bonusSubscriberFactory().Handle((ApplyForWageringRequirementBonus)message);
            else if (message is FulfillWageringRequirementBonus)
                _bonusSubscriberFactory().Handle((FulfillWageringRequirementBonus)message);
            else if (message is FundedIn)
                _bonusSubscriberFactory().Handle((FundedIn) message);
            else if (message is FundedOut)
                _bonusSubscriberFactory().Handle((FundedOut)message);
            else if (message is BetPlaced)
                _bonusSubscriberFactory().Handle((BetPlaced)message);
            else if (message is BetWon)
                _bonusSubscriberFactory().Handle((BetWon)message);
        }
    }
}