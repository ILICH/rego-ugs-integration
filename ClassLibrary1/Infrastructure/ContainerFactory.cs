﻿using ClassLibrary1.Payment;
using ClassLibrary1.Services.Wallet;
using Microsoft.Practices.Unity;
using REGO.Bonus;
using REGO.Game;

namespace ClassLibrary1
{
    class ContainerFactory
    {
        public UnityContainer CreateContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<BonusData>(new ContainerControlledLifetimeManager());
            container.RegisterType<GameData>(new ContainerControlledLifetimeManager());
            container.RegisterType<PaymentData>(new ContainerControlledLifetimeManager());
            return container;
        }
    }
}