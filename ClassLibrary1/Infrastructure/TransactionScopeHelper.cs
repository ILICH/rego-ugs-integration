using System.Transactions;

namespace REGO.Game
{
    class TransactionScopeHelper
    {
        public static TransactionScope CreateDefaultTransactionScope()
        {
            return new TransactionScope(); //simplest implementation possible, will be a bit different in a real app
        }
    }
}