﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REGO.Game
{
    class GameCommands
    {
        private readonly MessageBus _messageBus;
        private readonly GameData _gameData;

        public GameCommands(MessageBus messageBus, GameData gameData)
        {
            _messageBus = messageBus;
            _gameData = gameData;
        }

        public void PlaceBet(Guid betId, decimal amount)
        {
            EnsureEnoughBalance(amount);
            _gameData.Balance -= amount;
            _messageBus.Publish(new BetPlaced { Amount = amount });
        }

        public void WinBet(Guid betId, decimal amount)
        {
            _gameData.Balance += amount;
            _messageBus.Publish(new BetWon { Amount = amount });
        }

        public void LostBet(Guid betId)
        {
            _messageBus.Publish(new BetLost());
        }

        /// <summary>
        /// this method should be indempotent
        /// </summary>
        public void FundIn(Guid transactionId, decimal amount)
        {
            if (_gameData.TransactionsProcessed.Contains(transactionId)) return;
            _gameData.TransactionsProcessed.Add(transactionId);

            _gameData.Balance += amount;
            _messageBus.Publish(new FundedIn { Amount = amount, TransactionId = transactionId });
        }

        /// <summary>
        /// this method should be indempotent
        /// </summary>
        public void FundOut(Guid transactionId, decimal amount)
        {
            if (_gameData.TransactionsProcessed.Contains(transactionId)) return;
            EnsureEnoughBalance(amount);
            _gameData.TransactionsProcessed.Add(transactionId);

            _gameData.Balance -= amount;
            _messageBus.Publish(new FundedOut { Amount = amount });
        }

        private void EnsureEnoughBalance(decimal amount)
        {
            if (_gameData.Balance < amount) throw new ApplicationException("Not enough balance");
        }
    }
}