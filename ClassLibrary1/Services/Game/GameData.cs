using System;
using System.Collections.Generic;

namespace REGO.Game
{
    class GameData
    {
        public decimal Balance { get; set; }

        public List<Guid> TransactionsProcessed = new List<Guid>();
    }
}