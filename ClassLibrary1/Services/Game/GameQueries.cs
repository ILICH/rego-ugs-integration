namespace REGO.Game
{
    class GameQueries
    {
        private readonly GameData _gameData;

        public GameQueries(GameData gameData)
        {
            _gameData = gameData;
        }

        public decimal GetBalance()
        {
            return _gameData.Balance;
        }
    }
}