using System;

namespace REGO.Game
{
    class FundedIn : IEvent
    {
        public decimal  Amount;
        public Guid     TransactionId;
    }
}