using System;

namespace REGO.Game
{
    class FundedOut : IEvent
    {
        public decimal  Amount;
        public Guid     TransactionId;
    }
}