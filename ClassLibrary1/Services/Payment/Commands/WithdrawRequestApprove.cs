using System;
using REGO.Infrastructure;

namespace ClassLibrary1.Payment
{
    class WithdrawRequestApprove : ICommand
    {
        public decimal  Amount;
        public Guid     RequestId;
    }
}