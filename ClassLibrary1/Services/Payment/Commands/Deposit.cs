using System;
using REGO.Infrastructure;

namespace ClassLibrary1.Services.Payment.Commands
{
    class Deposit : ICommand
    {
        public decimal  Amount;
        public Guid     TransactionId;
    }
}