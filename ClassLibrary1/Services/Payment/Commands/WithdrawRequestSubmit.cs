using System;
using REGO.Infrastructure;

namespace ClassLibrary1.Payment
{
    class WithdrawRequestSubmit : ICommand
    {
        public decimal  Amount;
        public Guid     FundOutTransactionId;
        public Guid     LockId;
        public Guid     RequestId;
    }
}