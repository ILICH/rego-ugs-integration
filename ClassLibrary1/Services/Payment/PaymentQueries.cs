﻿namespace ClassLibrary1.Payment
{
    class PaymentQueries
    {
        private readonly PaymentData _paymentData;

        public PaymentQueries(PaymentData paymentData)
        {
            _paymentData = paymentData;
        }

        public decimal GetWithdrawalLockBalance()
        {
            return _paymentData.WithdrawalLockBalance;
        }
    }
}