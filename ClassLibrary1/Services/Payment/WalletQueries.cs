﻿using System;
using ClassLibrary1.Payment;
using REGO.Bonus;
using REGO.Game;

namespace ClassLibrary1.Services.Wallet
{
    class WalletQueries
    {
        private readonly GameQueries    _gameQueries;
        private readonly BonusQueries   _bonusQueries;
        private readonly PaymentQueries _paymentQueries;

        public WalletQueries(GameQueries gameQueries, BonusQueries bonusQueries, PaymentQueries paymentQueries)
        {
            _gameQueries = gameQueries;
            _bonusQueries = bonusQueries;
            _paymentQueries = paymentQueries;
        }

        public BalanceData GetBalanceData()
        {
            var gameBalance = _gameQueries.GetBalance();
            var bonusBalanceData = _bonusQueries.GetBonusBalanceData();
            var withdrawalLockBalance = _paymentQueries.GetWithdrawalLockBalance();

            var playableBalance = bonusBalanceData.Main + bonusBalanceData.Bonus;

            //in real application we should make multiple retries before firing exception
            if (gameBalance != playableBalance)
                throw new ApplicationException(
                    $"Playable balance is not consistent across the application. \n" +
                    $"gameBalance:{gameBalance} is not equal to mainBalance:{bonusBalanceData.Main} + bonusBalance:{bonusBalanceData.Bonus}");

            return new BalanceData
            {
                Playable = playableBalance,
                Total = playableBalance + withdrawalLockBalance,
                Main = bonusBalanceData.Main,
                Bonus = bonusBalanceData.Bonus,
                Free = playableBalance - bonusBalanceData.BonusLock,
                WithdrawalLock = withdrawalLockBalance,
                BonusLock = bonusBalanceData.BonusLock
            };
        }
    }
}