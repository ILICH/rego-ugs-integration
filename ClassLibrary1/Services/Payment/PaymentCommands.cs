﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1.Services.Payment.Commands;
using ClassLibrary1.Services.Wallet;
using REGO.Game;

namespace ClassLibrary1.Payment
{
    class PaymentCommands
    {
        private readonly MessageBus     _messageBus;
        private readonly PaymentData    _paymentData;
        private readonly WalletQueries _walletQueries;

        public PaymentCommands(MessageBus messageBus, PaymentData paymentData, WalletQueries walletQueries)
        {
            _messageBus = messageBus;
            _paymentData = paymentData;
            _walletQueries = walletQueries;
        }

        public void Deposit(decimal amount)
        {
            _messageBus.Publish(new Deposit { Amount = amount, TransactionId = Guid.NewGuid()});
        }

        public void WithdrawRequestSubmit(Guid withdrawRequestId, decimal amount)
        {
            var balanceData = _walletQueries.GetBalanceData();
            if (balanceData.Free < amount)
                throw new ApplicationException("Insufficient funds on Free balance");

            _messageBus.Publish(new WithdrawRequestSubmit
            {
                RequestId = withdrawRequestId,
                Amount = amount,
                LockId = Guid.NewGuid(),
                FundOutTransactionId = Guid.NewGuid()
            });
        }

        public void WithdrawRequestApprove(Guid requestId)
        {
            _messageBus.Publish(new WithdrawRequestApprove
            {
                RequestId = requestId,
            });
        }

        public void WithdrawRequestReject(decimal amount)
        {
            _messageBus.Publish(new WithdrawRequestRejected { Amount = amount });
        }
    }
}
