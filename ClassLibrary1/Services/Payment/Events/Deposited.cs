using System;

namespace REGO.Game
{
    class Deposited : IEvent
    {
        public Guid     TransactionId;
        public decimal  Amount;
    }
}