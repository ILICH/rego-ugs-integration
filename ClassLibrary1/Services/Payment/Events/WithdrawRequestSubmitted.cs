using System;

namespace REGO.Game
{
    class WithdrawRequestSubmitted : IEvent
    {
        public decimal  Amount;
        public Guid     FundOutTransactionId;
        public Guid     LockId;
    }
}