using System;

namespace REGO.Game
{
    class WithdrawRequestApproved : IEvent
    {
        public decimal  Amount;
        public Guid     RequestId;
    }

    class WithdrawRequestRejected : IEvent
    {
        public decimal Amount;
    }
}