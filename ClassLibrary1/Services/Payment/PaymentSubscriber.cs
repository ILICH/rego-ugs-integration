using System.Linq;
using System.Security.Claims;
using ClassLibrary1.Services.Payment.Commands;
using ClassLibrary1.Services.Wallet;
using REGO.Game;

namespace ClassLibrary1.Payment
{
    internal class PaymentSubscriber
    {
        private readonly GameCommands   _gameCommands;
        private readonly MessageBus     _messageBus;
        private readonly PaymentData _paymentData;

        public PaymentSubscriber(GameCommands gameCommands, MessageBus messageBus, PaymentData paymentData)
        {
            _gameCommands = gameCommands;
            _messageBus = messageBus;
            _paymentData = paymentData;
        }

        public void Handle(Deposit command)
        {
            //FundIn should be indempotent
            _gameCommands.FundIn(command.TransactionId, command.Amount);

            _messageBus.Publish(new Deposited
            {
                Amount = command.Amount,
                TransactionId = command.TransactionId,
            });
        }

        public void Handle(WithdrawRequestSubmit command)
        {
            //move money out of Game service
            _gameCommands.FundOut(command.FundOutTransactionId, command.Amount);

            //create request record to track all pending requests
            _paymentData.WithdrawRequests.Add(new WithdrawRequest {Id = command.RequestId, Amount = command.Amount, LockId = command.LockId, Status = WithdrawRequestStatus.Submitted});

            //lock money for withdrawal in our wallet
            _paymentData.WithdrawalLocks.Add(command.LockId, new WithdrawalLock { Id = command.LockId, Amount = command.Amount });
            _paymentData.WithdrawalLockBalance += command.Amount;

            _messageBus.Publish(new WithdrawRequestSubmitted
            {
                Amount = command.Amount,
                LockId = command.LockId,
                FundOutTransactionId = command.FundOutTransactionId
            });

            //TODO: there is still as small chance that some amount will be spent in Game subdomain 
            //TODO: after we performed balance check and before money removed from Game balance
            //it means that we need to detect such situation by performing additional validations after WithdrawRequestSubmitted event
            //and automatically reject withdrawal request if some inconsistencies were found
        }

        public void Handle(WithdrawRequestApprove command)
        {
            var request = _paymentData.WithdrawRequests.Where(x => x.Id == command.RequestId && x.Status == WithdrawRequestStatus.Submitted).Single();

            //unlock money from our wallet, meaning money will disappear from all balances
            _paymentData.WithdrawalLocks.Remove(request.LockId);
            _paymentData.WithdrawalLockBalance -= request.Amount;

            request.Status = WithdrawRequestStatus.Approved;

            _messageBus.Publish(new WithdrawRequestApproved
            {
                Amount = request.Amount,
                RequestId = request.Id
            });
        }
    }
}