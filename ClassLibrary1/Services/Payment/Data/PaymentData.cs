﻿using System;
using System.Collections.Generic;

namespace ClassLibrary1.Payment
{
    class PaymentData
    {
        /// <summary>
        /// Balance which is locked by Payment system in order to be withdrawn. 
        /// This amount was Funded Out of the Game subdomain in order to prevent it from being spent on wagers.
        /// </summary>
        public decimal WithdrawalLockBalance;
        public List<WithdrawRequest> WithdrawRequests = new List<WithdrawRequest>();
        public Dictionary<Guid, WithdrawalLock> WithdrawalLocks = new Dictionary<Guid, WithdrawalLock>();
    }

    class WithdrawalLock
    {
        public Guid     Id;
        public decimal  Amount;
    }
}