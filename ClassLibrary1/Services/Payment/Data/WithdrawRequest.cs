﻿using System;

namespace ClassLibrary1.Payment
{
    class WithdrawRequest
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public Guid LockId { get; set; }

        public WithdrawRequestStatus Status { get; set; }
    }

    enum WithdrawRequestStatus
    {
        Submitted,
        Approved
    }
}