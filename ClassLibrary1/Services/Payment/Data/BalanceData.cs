﻿namespace ClassLibrary1.Services.Wallet
{
    class BalanceData
    {
        public decimal Total;
        public decimal Playable;
        public decimal Main;
        public decimal Free;
        public decimal Bonus;
        public decimal WithdrawalLock;
        public decimal BonusLock;
    }
}