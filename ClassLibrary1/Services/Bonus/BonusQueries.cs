namespace REGO.Bonus
{
    class BonusQueries
    {
        private readonly BonusData _bonusData;

        public BonusQueries(BonusData bonusData)
        {
            _bonusData = bonusData;
        }

        public BonusBalanceData GetBonusBalanceData()
        {
            return new BonusBalanceData
            {
                Bonus = _bonusData.BonusBalance,
                Main = _bonusData.MainBalance,
                BonusLock = _bonusData.BonusLockBalance
            };
        }
    }
}