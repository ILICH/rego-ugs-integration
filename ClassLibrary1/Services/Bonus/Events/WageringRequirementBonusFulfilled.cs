using System;
using REGO.Game;

namespace REGO.Bonus
{
    class WageringRequirementBonusFulfilled : IEvent
    {
        public Guid     BonusRedemptionId;

        /// <summary>
        /// Amount that could be potentially released as a result of fullfilling WR bonus
        /// </summary>
        public decimal  MaxReleaseAmount { get; set; }

        /// <summary>
        /// Amount actually being released taking into consideration current bonus balance
        /// </summary>
        public decimal ActualReleaseAmount { get; set; }
    }
}