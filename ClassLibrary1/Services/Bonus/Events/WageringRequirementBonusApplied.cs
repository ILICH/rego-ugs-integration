using System;
using REGO.Game;

namespace REGO.Bonus
{
    class WageringRequirementBonusApplied : IEvent
    {
        public Guid     BonusRedemptionId;
        public Guid     FundInTransactionId;
        public decimal  Amount;
        public decimal  BonusBalance;
        public decimal  BonusLockBalance;
    }
}