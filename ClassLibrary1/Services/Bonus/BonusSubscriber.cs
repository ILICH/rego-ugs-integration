using System;
using System.Transactions;
using REGO.Game;
using ClassLibrary1.Services.Bonus.Commands;
using ClassLibrary1.Services.Wallet;

namespace REGO.Bonus
{
    internal class BonusSubscriber
    {
        private readonly MessageBus     _messageBus;
        private readonly GameCommands   _gameCommands;
        private readonly BonusData      _bonusData;

        public BonusSubscriber(
            MessageBus messageBus,
            GameCommands gameCommands,
            BonusData walletData)
        {
            _messageBus = messageBus;
            _gameCommands = gameCommands;
            _bonusData = walletData;
        }

        public void Handle(ApplyForWageringRequirementBonus command)
        {
            //using scope is important here because we need to rollback increments of a balances in Bonus subsystem in case of the exception
            using (var scope = TransactionScopeHelper.CreateDefaultTransactionScope())
            {
                _bonusData.WageringRequirementBonus.IsEnabled = true;
                _bonusData.WageringRequirementBonus.RedemptionId = Guid.NewGuid();
                _bonusData.WageringRequirementBonus.TargetAmount = command.ShouldBeWageredAmount;
                _bonusData.WageringRequirementBonus.BonusAmount = command.BonusAmount;
                _bonusData.WageringRequirementBonus.LockId = command.BonusLockId;

                //when we applied for this bonus - money moved into Bonus balance
                _bonusData.BonusBalance += command.BonusAmount;
                _bonusData.FundTransactions.Add(command.FundInTransactionId, new FundTransaction {Id = command.FundInTransactionId, Amount = command.BonusAmount, Type = FundTransactionType.FundIn});
                //bonus lock should be placed for the same amount as well
                _bonusData.BonusLockBalance += command.BonusAmount;
                _bonusData.BonusLocks.Add(command.BonusLockId, new BonusLock { Amount = command.BonusAmount, Id = command.BonusLockId });
                //this balance should become available for wagering, so we're making this amount visible to Game subdomain
                _gameCommands.FundIn(command.FundInTransactionId, command.BonusAmount);

                _messageBus.Publish(new WageringRequirementBonusApplied
                {
                    Amount = command.BonusAmount,
                    FundInTransactionId = Guid.NewGuid(),
                    BonusRedemptionId = _bonusData.WageringRequirementBonus.RedemptionId,
                    BonusBalance = _bonusData.BonusBalance,
                    BonusLockBalance = _bonusData.BonusLockBalance
                });

                scope.Complete();
            }
        }

        public void Handle(BetPlaced @event)
        {
            DeductMoneyFromGameBalances(@event.Amount);

            if (_bonusData.WageringRequirementBonus.IsEnabled)
                ProcessWageringRequirement(@event.Amount);
        }

        private void DeductMoneyFromGameBalances(decimal amount)
        {
            if (_bonusData.BonusBalance >= amount)
                _bonusData.BonusBalance -= amount;
            else
                _bonusData.MainBalance -= amount;
        }

        private void ProcessWageringRequirement(decimal betAmount)
        {
            //for each bet placed we have to increase wagered amount
            _bonusData.WageringRequirementBonus.WageredAmount += betAmount;

            if (_bonusData.WageringRequirementBonus.WageredAmount >= _bonusData.WageringRequirementBonus.TargetAmount)
            {
                _messageBus.Publish(new FulfillWageringRequirementBonus
                {
                    BonusAmount = _bonusData.WageringRequirementBonus.BonusAmount,
                    BonusLockId = _bonusData.WageringRequirementBonus.LockId,
                    WonAmountBonusPart = _bonusData.WageringRequirementBonus.WonAmountBonusPart,
                    WageredAmountBonusPart = _bonusData.WageringRequirementBonus.WageredAmountBonusPart
                });
            }
        }

        public void Handle(FulfillWageringRequirementBonus command)
        {
            ResetWageringRequirementData();

            //calculating actual release amount: current bonus balance can be lower than max release amount, depending on current bonus balance
            var maxReleaseAmount = command.WonAmountBonusPart - command.WageredAmountBonusPart + command.BonusAmount;
            var actualReleaseAmount = Math.Min(maxReleaseAmount, _bonusData.BonusBalance);
            if (actualReleaseAmount > 0)
            {
                _bonusData.BonusBalance -= actualReleaseAmount;
                _bonusData.MainBalance += actualReleaseAmount;
            }

            //remove amount locked from being withdrawn
            _bonusData.BonusLockBalance -= command.BonusAmount;
            _bonusData.BonusLocks.Remove(command.BonusLockId);

            _messageBus.Publish(new WageringRequirementBonusFulfilled
            {
                BonusRedemptionId = _bonusData.WageringRequirementBonus.RedemptionId,
                MaxReleaseAmount = maxReleaseAmount, 
                ActualReleaseAmount = actualReleaseAmount
            });
        }

        public void Handle(BetWon betWon)
        {
            //Accumulate won amount to transfer it from bonus balance into the main balance after WR fullfillment.
            if (_bonusData.WageringRequirementBonus.IsEnabled)
            {
                var wonAmountBonusPart = CalculateBonusWonPart(betWon);
                _bonusData.WageringRequirementBonus.WonAmountBonusPart += wonAmountBonusPart;
            }

            _bonusData.MainBalance += betWon.Amount;
        }

        private decimal CalculateBonusWonPart(BetWon betWon)
        {
            //TODO: calculate won amount based on amount taken from bonus balance only while betting and disregard amount taken from main balance.
            //use proportion to calculate amount. 
            return betWon.Amount;
        }

        public void Handle(FundedIn @event)
        {
            if (_bonusData.FundTransactions.ContainsKey(@event.TransactionId))
                return; //this was fund-in from bonus system and we already added it to one of our balances
            _bonusData.FundTransactions.Add(@event.TransactionId, new FundTransaction { Id = @event.TransactionId, Amount = @event.Amount, Type = FundTransactionType.FundIn});
            _bonusData.MainBalance += @event.Amount;
        }

        public void Handle(FundedOut @event)
        {
            if (_bonusData.FundTransactions.ContainsKey(@event.TransactionId))
                return; //this was fund-out from bonus system and we already substracted it from one of our balances
            _bonusData.FundTransactions.Add(@event.TransactionId, new FundTransaction { Id = @event.TransactionId, Amount = @event.Amount, Type = FundTransactionType.FundOut });
            _bonusData.MainBalance -= @event.Amount;
        }

        private void ResetWageringRequirementData()
        {
            _bonusData.WageringRequirementBonus.IsEnabled = false;
            _bonusData.WageringRequirementBonus.TargetAmount = 0;
            _bonusData.WageringRequirementBonus.WageredAmount = 0;
            _bonusData.WageringRequirementBonus.WageredAmountBonusPart = 0;
            _bonusData.WageringRequirementBonus.WonAmountBonusPart = 0;
        }
    }
}