using System;
using System.Collections.Generic;
using ClassLibrary1.Services.Bonus.Commands;
using REGO.Game;

namespace REGO.Bonus
{
    class BonusCommands
    {
        private readonly MessageBus                 _messageBus;
        private readonly BonusData                  _bonusData;

        public BonusCommands(MessageBus messageBus, BonusData bonusData)
        {
            _messageBus = messageBus;
            _bonusData = bonusData;
        }

        public void ApplyForWageringRequirementBonus(decimal shouldBeWagered, decimal bonusAmount)
        {
            _messageBus.Publish(new ApplyForWageringRequirementBonus
            {
                ShouldBeWageredAmount = shouldBeWagered,
                BonusAmount = bonusAmount,
                FundInTransactionId = Guid.NewGuid(),
                BonusLockId = Guid.NewGuid()
            });
        }
    }
}