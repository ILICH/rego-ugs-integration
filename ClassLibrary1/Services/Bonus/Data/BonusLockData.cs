using System;

namespace REGO.Bonus
{
    class BonusLock
    {
        public Guid Id;
        public decimal Amount;
    }
}