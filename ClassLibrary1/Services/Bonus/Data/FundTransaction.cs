using System;

namespace REGO.Bonus
{
    class FundTransaction
    {
        public Guid                 Id;
        public FundTransactionType  Type;
        public decimal              Amount;
    }

    enum FundTransactionType
    {
        FundIn,
        FundOut
    }
}