using System;
using System.Collections.Generic;

namespace REGO.Bonus
{
    class WageringRequirementBonus
    {
        public bool     IsEnabled = false;
        public Guid     RedemptionId;
        public decimal  BonusAmount; //this will be credited to bonus wallet when applying for the bonus
        public decimal  TargetAmount; //this should be wagered in order to unlock money from bonus balance to be available for withdrawal
        public decimal  WageredAmount; //amount which was already wagered
        public decimal  WageredAmountBonusPart; //amount which was wagered from bonus balance only. Is equal or less than WageredAmount.
        public decimal  WonAmountBonusPart; //amount won as a result of being wagered from bonus balance (part of proportion)
        public Guid     LockId; //id of the bonus amount lock 

        public List<Guid> FulfilledRedemptions = new List<Guid>();
    }
}