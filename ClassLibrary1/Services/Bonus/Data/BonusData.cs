using System;
using System.Collections.Generic;

namespace REGO.Bonus
{
    class BonusData
    {
        //part of playable balance which could be withdrawn
        public decimal MainBalance;

        /// <summary>
        /// Bonus Lock Balance is a non-withdrawable amount of money which you can still use for wagering
        /// </summary>
        public decimal BonusBalance;

        /// <summary>
        /// Bonus Lock Balance is a non-withdrawable locked amount of money. Can be unlocked when WR is fullfilled or when balance goes below certain threshold
        /// </summary>
        public decimal BonusLockBalance; 

        /// <summary>
        /// Tracking of Fund Transactions needed in order to distinquish between our fund transactions and all others which may be triggered by other components.
        /// We are subscribing to Fund events (FundedIn and FundedOut) and making sure that game balance in Bonus subdomain is eventually the same as in Game subdomain (maybe with some small delays)
        /// </summary>
        public Dictionary<Guid, FundTransaction>    FundTransactions = new Dictionary<Guid, FundTransaction>();

        public WageringRequirementBonus             WageringRequirementBonus = new WageringRequirementBonus();
        public Dictionary<Guid, BonusLock>      BonusLocks = new Dictionary<Guid, BonusLock>();
    }

}