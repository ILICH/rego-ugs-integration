namespace REGO.Bonus
{
    class BonusBalanceData
    {
        public decimal Main;
        public decimal Bonus;

        /// <summary>
        /// Bonus lock balance initially equals to Bonus Balance.
        /// As Player keeps wagering money from Bonus balance, BonusLockBalance remains the same and reduced only when balance goes below wagering requirement theshold.
        /// </summary>
        public decimal BonusLock; 
    }
}