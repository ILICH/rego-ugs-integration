using System;
using REGO.Infrastructure;

namespace ClassLibrary1.Services.Bonus.Commands
{
    class FulfillWageringRequirementBonus : ICommand
    {
        public decimal  WonAmountBonusPart { get; set; }
        public decimal  BonusAmount { get; set; }
        public Guid     BonusLockId { get; set; }
        public decimal  WageredAmountBonusPart { get; set; }
    }
}