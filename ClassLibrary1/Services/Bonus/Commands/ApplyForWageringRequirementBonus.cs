﻿using System;
using REGO.Infrastructure;

namespace ClassLibrary1.Services.Bonus.Commands
{
    class ApplyForWageringRequirementBonus : ICommand
    {
        public decimal  ShouldBeWageredAmount { get; set; }
        public decimal  BonusAmount { get; set; }
        public Guid     BonusLockId { get; set; }
        public Guid     FundInTransactionId { get; set; }
    }
}