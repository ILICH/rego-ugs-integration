﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1.Payment;
using ClassLibrary1.Services.Wallet;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using REGO.Bonus;
using REGO.Game;

namespace ClassLibrary1
{
    [TestFixture]
    class Tests
    {
        private UnityContainer _container;
        [SetUp]
        public void BeforeEach()
        {
            _container = new ContainerFactory().CreateContainer();
        }

        [Test]
        public void Deposit_play_bonus_withdraw_scenario()
        {
            var bonusCommands = _container.Resolve<BonusCommands>();
            var paymentCommands = _container.Resolve<PaymentCommands>();
            var gameCommands = _container.Resolve<GameCommands>();
            var walletQueries = _container.Resolve<WalletQueries>();

            bonusCommands.ApplyForWageringRequirementBonus(shouldBeWagered: 20, bonusAmount: 25);
            Assert.That(walletQueries.GetBalanceData().Total, Is.EqualTo(25));

            paymentCommands.Deposit(amount: 500);
            Assert.That(walletQueries.GetBalanceData().Total, Is.EqualTo(525));

            var betId = Guid.NewGuid();
            gameCommands.PlaceBet(betId, amount: 10);
            gameCommands.WinBet(betId, amount: 100);
            Assert.That(walletQueries.GetBalanceData().Total, Is.EqualTo(615)); //615 = 525 - 10 + 100

            betId = Guid.NewGuid();
            gameCommands.PlaceBet(betId, amount: 20);
            gameCommands.LostBet(betId);
            Assert.That(walletQueries.GetBalanceData().Total, Is.EqualTo(595));

            var withdrawRequestId = Guid.NewGuid();
            paymentCommands.WithdrawRequestSubmit(withdrawRequestId, amount: 595);
            Assert.That(walletQueries.GetBalanceData().Playable, Is.EqualTo(0));
            Assert.That(walletQueries.GetBalanceData().Total, Is.EqualTo(595));

            paymentCommands.WithdrawRequestApprove(withdrawRequestId);

            var balanceData = walletQueries.GetBalanceData();
            Assert.That(balanceData.Total, Is.EqualTo(0));
            Assert.That(balanceData.Playable, Is.EqualTo(0));
            Assert.That(balanceData.Bonus, Is.EqualTo(0));
            Assert.That(balanceData.Main, Is.EqualTo(0));
            Assert.That(balanceData.BonusLock, Is.EqualTo(0));
            Assert.That(balanceData.WithdrawalLock, Is.EqualTo(0));
        }

        [Test]
        public void Can_deposit_money()
        {
            var paymentCommands = _container.Resolve<PaymentCommands>();
            var walletQueries = _container.Resolve<WalletQueries>();

            paymentCommands.Deposit(amount: 500);

            var balanceData = walletQueries.GetBalanceData();
            Assert.That(balanceData.Total, Is.EqualTo(500));
            Assert.That(balanceData.Playable, Is.EqualTo(500));
            Assert.That(balanceData.Bonus, Is.EqualTo(0));
            Assert.That(balanceData.Main, Is.EqualTo(500));
            Assert.That(balanceData.BonusLock, Is.EqualTo(0));
            Assert.That(balanceData.WithdrawalLock, Is.EqualTo(0));
        }

        [Test]
        public void Can_apply_for_wagering_requirement()
        {
            var bonusCommands = _container.Resolve<BonusCommands>();
            var walletQueries = _container.Resolve<WalletQueries>();

            bonusCommands.ApplyForWageringRequirementBonus(shouldBeWagered: 20, bonusAmount: 25);

            var balanceData = walletQueries.GetBalanceData();
            Assert.That(balanceData.Total, Is.EqualTo(25));
            Assert.That(balanceData.Playable, Is.EqualTo(25));
            Assert.That(balanceData.Bonus, Is.EqualTo(25));
            Assert.That(balanceData.Main, Is.EqualTo(0));
            Assert.That(balanceData.BonusLock, Is.EqualTo(25));
            Assert.That(balanceData.WithdrawalLock, Is.EqualTo(0));
        }
    }
}
